let buttons = document.getElementsByClassName("number")
let previousOp = "";
let display = "";
let currentNum = "";
let total = "";

const calc = function(sum, currNum, operator) {
    switch (operator) {
        case '+':
            return parseFloat(Number(sum) + Number(currNum));
        case '-':
            return parseFloat(Number(sum) - Number(currNum));
        case '*':
            return parseFloat(Number(sum) * Number(currNum));
        case '/':
            return parseFloat(Number(sum) / Number(currNum));
    }
} 


/**
 * Validating if a dot can be added.
 * (\.([\x30-\x39]*))$ check if last number in current formula 
 * already has a dot followed by 0 or more numbers.
 * [\x2A\x2B\x2D\x2F]$ checks if there is an operator in the 
 * end of current formula
 */

const validateOp = function(str) {
    return !(/[\x2A\x2B\x2D-\x2F]$/.test(str));
} 
const validateDot = function(str) {
    return !(/(\.([\x30-\x39]*))$/.test(str)) && validateOp(str);
} 
const validateEnter = function(str) {
    return (/[\x30-\x39]+[\x2A\x2B\x2D-\x2F][\x30-\x39]+$/.test(str))
}


document.addEventListener("click", function(event) {
    event.preventDefault()
    if (event.target.matches(".number")){

        if (previousOp !== "" && display !== "") {
            currentNum += event.target.outerText;
            display += event.target.outerText;
            let show = document.getElementById("display");
            show.innerHTML = display;
        } else {
            currentNum += event.target.outerText
            display = currentNum;
            total = currentNum;
            let show = document.getElementById("display");
            show.innerHTML = display;
        }
    } else if (event.target.matches(".operator")) {
        if (validateOp(display) && display) {
            currentNum = "";
            if(previousOp) total = calc(total, currentNum, previousOp);
            previousOp = event.target.outerText;
            display += previousOp;
            let show = document.getElementById("display");
            show.innerHTML = display;
        }
    } else if (event.target.matches(".equal")) {
        if (validateEnter(display)) {
            total = calc(total, currentNum, previousOp)
            let show = document.getElementById("display");
            display = total.toString()
            show.innerHTML = total;
        }
    } else if (event.target.matches("#decimal")) {

        if(validateDot(display) && display) {
            currentNum += event.target.outerText;
            display += event.target.outerText;
            let show = document.getElementById("display");
            show.innerHTML = display;
        }
    } else if (event.target.matches("#clear")) {
        total, previousOp, currentNum = "";
        display = "0";
        let show = document.getElementById("display");
        show.innerHTML = display;
        display = "";
    } else return;
}, false)

document.addEventListener("keydown", function(event) {
    event.preventDefault()
    // console.log(event.key, event.code)
    if (/[0-9]/.test(event.key)){

        if (previousOp !== "" && display !== "") {
            currentNum += event.key;
            display += event.key;
            let show = document.getElementById("display");
            show.innerHTML = display;
        } else {
            currentNum += event.key
            display = currentNum;
            total = currentNum;
            let show = document.getElementById("display");
            show.innerHTML = display;
        }
    } else if (/[\x2A\x2B\x2D\x2F]/.test(event.key)) {
        if (validateOp(display) && display) {
            currentNum = "";
            if(previousOp) total = calc(total, currentNum, previousOp);
            previousOp = event.key;
            display += previousOp;
            let show = document.getElementById("display");
            show.innerHTML = display;
        }
    } else if (/[\x3D]/.test(event.key)||/Enter/i.test(event.code)) {
        if (validateEnter(display)) {
            total = calc(total, currentNum, previousOp)
            let show = document.getElementById("display");
            display = total.toString()
            show.innerHTML = total;
        }
    } else if (/[\x2E]/.test(event.key)) {

        if(validateDot(display) && display) {
            currentNum += event.key;
            display += event.key;
            let show = document.getElementById("display");
            show.innerHTML = display;
        }
    } else if (/Backspace/i.test(event.code)) {
        
            total, previousOp, currentNum = "";
            display = "0";
            let show = document.getElementById("display");
            show.innerHTML = display;
            display = "";
    
    } else return;
}, false)

// TODO: after hitting enter and adding a number there is an error, fix it
