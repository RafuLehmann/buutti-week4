import axios from 'axios';

let print = console.log
const todosUrl = 'https://jsonplaceholder.typicode.com/todos/';
const specTodo = 'https://jsonplaceholder.typicode.com/todos/1';
const usersApi = 'https://jsonplaceholder.typicode.com/users/';


const fetchData = async (url) => {
    try {
        const response = await axios.get(url);
        return response.data;
    } catch (err) {
        print("fetchApiError:", err.message);
    }
}

// Getting details directly from a specific users api endpoint.
// Then matching user id and adding details to the fetched todo list. 
const addUserDetailsFromApi = async (selectedTodo) => {
    const url = `${usersApi}${selectedTodo.userId}`;
    let userDetails =  await fetchData(url)
    if (selectedTodo.userId) delete selectedTodo.userId;
    selectedTodo.user = userDetails;
    return selectedTodo;
}

// Fetch all users at once 
const allUsers = await fetchData(usersApi);

// Reducing the amount of apicalls by using the already fetched list of users.
// Then matching user id and adding details to the fetched todo list. 
const addUserDetailsFromLocal = async (selectedTodo) => {
    print(allUsers.length)
    let userDetails =  allUsers.find(item => item.id === selectedTodo.userId);
    if (selectedTodo.userId) delete selectedTodo.userId;
    selectedTodo.user = userDetails;
    return selectedTodo;
}


const combineDbs = async (modifyApi) => {
    const todosData = await fetchData(modifyApi);

    if(todosData.length > 1) {
        const modified =  await Promise.all( todosData.map(async (todos) => {
            // const result = await addUserDetailsFromApi(todos);
            const result = await addUserDetailsFromLocal(todos);
            return result;
        }));
        return modified;
    } else {
        // const modified = await addUserDetailsFromApi(todosData);
        const modified = await addUserDetailsFromLocal(todosData);
        return [modified];
    }
}

// function that removes all other keys exept name, username and email
const removeFieldsFromUser = async (fields, object) => {
    for (let i = 0; i < object.length; i++) {
        const keys = Object.keys(object[i].user).filter((item) => !fields.includes(item))
        keys.map(key => {
            delete object[i].user[key];
        })
    }

    return object;
}

const fieldsToKeep = ['name', 'username', 'email'];
const fetched = await combineDbs(specTodo);
const cleaned = await removeFieldsFromUser(fieldsToKeep, fetched)  
console.log(cleaned) 
