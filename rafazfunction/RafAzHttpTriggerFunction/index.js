module.exports = async function (context, req) {
    context.log('Function triggered.');

    const first = req.query.first;
    const second = req.body.second;

    if (!first || !second) {
        context.res = {
            status: 400, /* Defaults to 200 */
            body: `Bad request with body parameters: ${first} and ${second}`
        };
    } else {
        const sum = first + second;
        context.res = {
            status: 200, /* Defaults to 200 */
            body: `Calculated sum is ${sum}`
        };
    }
}